/*
  This project file is derived from the OLED_LoRa_Receiver example
  included with the Heltec libraries.
  https://github.com/Heltec-Aaron-Lee/WiFi_Kit_series
*/
#include "heltec.h" 
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include "WiFiConfig.h"
#include <HTTPClient.h>

#define BAND    915E6  //you can set band here directly,e.g. 868E6,915E6
#define BW      250E3

String packSize = "--";
String packet ;
uint64_t chipId = 0;
String id = "";
String mac = "";
uint16_t high = 0;
uint32_t low = 0;
HTTPClient http;
WiFiClientSecure client;
int counter = 1;


void ConnectToWiFi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, WiFiPassword);
  Serial.print("Connecting to "); Serial.println(SSID);
 
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print('.');
    delay(500);
 
    if ((++i % 16) == 0)
    {
      Serial.println(F(" still trying to connect"));
    }
  }
  Serial.print(("Connected. My IP address is: "));
  Serial.println(WiFi.localIP());
}

void LoRaData(){

  mac = packet.substring(0,12);
  id = packet.substring(12,18);

  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->drawString(0, 0, "Id: " + String(id));
  Heltec.display->drawString(0, 10, "Received: "+ packSize + " bytes");
  Heltec.display->drawStringMaxWidth(0 , 20 , 128, packet);
  Heltec.display->drawString(0, 40, "Counter: "+ String(counter, DEC) + " packets");
  counter = counter + 1;

  Heltec.display->display();
  
  http.addHeader("Content-Type", "application/json");
  String httpRequestData = "{\"mac\":\"" + mac + "\",\"id\":\"" + id + "\"}"; 
  Serial.println(httpRequestData);

  int httpResponseCode = http.POST(httpRequestData);          

  Serial.print("HTTP Response Code: ");
  Serial.println(httpResponseCode);

  if (httpResponseCode!=200) {
    Serial.println("Restarting session");
    http.end();
    http.begin(client, ServerName);
  }
}

void cbk(int packetSize) {
  packet ="";
  packSize = String(packetSize,DEC);
  for (int i = 0; i < packetSize; i++) { packet += (char) LoRa.read(); }
  LoRaData();
}

void setup() { 
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Heltec.display->init();
  Heltec.display->flipScreenVertically();  
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->clear();
  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");
  Heltec.display->drawString(0, 10, "Wait for incoming data...");
  Heltec.display->display();
//  LoRa.onReceive(cbk);
  LoRa.setSpreadingFactor(7);
  LoRa.setSignalBandwidth(BW);
  LoRa.receive(18);
  ConnectToWiFi();
  http.begin(client, ServerName);
}

void loop() {
  int packetSize = LoRa.parsePacket(18);
  if (packetSize) { 
    cbk(packetSize);  
  }
}
