/* 
  Derived from the work here:
  https://github.com/ESP-EOS/ESP32-WiFi-Sniffer
 */
#include "freertos/FreeRTOS.h"
#include "esp_wifi.h"
#include "esp_wifi_types.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "heltec.h"

#define LED_GPIO_PIN                     5
#define WIFI_CHANNEL_SWITCH_INTERVAL  (500)
#define WIFI_CHANNEL_MAX               (11)
#define BAND                          915E6
#define BW                            250E3
#define MAC_SIZE                         12

uint32_t counter;
uint8_t level = 0, channel = 1;
String macs_seen[500];
String mac;
uint32_t len = 0;
uint64_t chipId = 0;
String id = "";
uint16_t high = 0;
uint32_t low = 0;

static wifi_country_t wifi_country = {.cc="US", .schan = 1, .nchan = 11}; //Most recent esp32 library struct

typedef struct {
  unsigned frame_ctrl:16;
  unsigned duration_id:16;
  uint8_t addr1[6]; /* receiver address */
  uint8_t addr2[6]; /* sender address */
  uint8_t addr3[6]; /* filtering address */
  unsigned sequence_ctrl:16;
  uint8_t addr4[6]; /* optional */
} wifi_ieee80211_mac_hdr_t;

typedef struct {
  wifi_ieee80211_mac_hdr_t hdr;
  uint8_t payload[0]; /* network data ended with 4 bytes csum (CRC32) */
} wifi_ieee80211_packet_t;

static esp_err_t event_handler(void *ctx, system_event_t *event);
static void wifi_sniffer_init(void);
static void wifi_sniffer_set_channel(uint8_t channel);
static const char *wifi_sniffer_packet_type2str(wifi_promiscuous_pkt_type_t type);
static void wifi_sniffer_packet_handler(void *buff, wifi_promiscuous_pkt_type_t type);

esp_err_t event_handler(void *ctx, system_event_t *event)
{
  return ESP_OK;
}

void wifi_sniffer_init(void)
{
  nvs_flash_init();
  tcpip_adapter_init();
  ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
  ESP_ERROR_CHECK( esp_wifi_set_country(&wifi_country) ); /* set country for channel range [1, 13] */
  ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_NULL) );
  ESP_ERROR_CHECK( esp_wifi_start() );
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_promiscuous_rx_cb(&wifi_sniffer_packet_handler);
}

void wifi_sniffer_set_channel(uint8_t channel)
{
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
}

const char * wifi_sniffer_packet_type2str(wifi_promiscuous_pkt_type_t type)
{
  switch(type) {
  case WIFI_PKT_MGMT: return "MGMT";
  case WIFI_PKT_DATA: return "DATA";
  default:  
  case WIFI_PKT_MISC: return "MISC";
  }
}

void wifi_sniffer_packet_handler(void* buff, wifi_promiscuous_pkt_type_t type)
{
  if (type != WIFI_PKT_MGMT)
    return;

  const wifi_promiscuous_pkt_t *ppkt = (wifi_promiscuous_pkt_t *)buff;
  const wifi_ieee80211_packet_t *ipkt = (wifi_ieee80211_packet_t *)ppkt->payload;
  const wifi_ieee80211_mac_hdr_t *hdr = &ipkt->hdr;

  String p_type = wifi_sniffer_packet_type2str(type);
  uint8_t chan = ppkt->rx_ctrl.channel;
  uint8_t addr2_0 = hdr->addr2[0];
  uint8_t addr2_1 = hdr->addr2[1];  
  uint8_t addr2_2 = hdr->addr2[2];
  uint8_t addr2_3 = hdr->addr2[3];  
  uint8_t addr2_4 = hdr->addr2[4];
  uint8_t addr2_5 = hdr->addr2[5];
  String mac_to_send = "";
  String id_to_send;


  LoRa.beginPacket(12);
  LoRa.setTxPower(20,RF_PACONFIG_PASELECT_PABOOST);
  LoRa.setSpreadingFactor(7);
  LoRa.setSignalBandwidth(BW);
  LoRa.printf("%02x%02x%02x%02x%02x%02x",
    addr2_0,
    addr2_1,
    addr2_2,
    addr2_3,
    addr2_4,
    addr2_5
  );    
//  Serial.printf("%02x%02x%02x%02x%02x%02x\n",
//    addr2_0,
//    addr2_1,
//    addr2_2,
//    addr2_3,
//    addr2_4,
//    addr2_5
//  );  
  id_to_send = id.substring(6, 12);
  LoRa.print(id_to_send);
  LoRa.endPacket(true);
}

// the setup function runs once when you press reset or power the board
void setup() {
  chipId=ESP.getEfuseMac();
  high = ("%04X",(uint16_t)(chipId>>32));//print High 2 bytes
  low = ("%08X",(uint32_t)chipId);//print Low 4bytes.
  char buf_high[4];
  char buf_low[8];
  sprintf(buf_high, "%04x", high);
  id += buf_high; 
  sprintf(buf_low, "%08x", low);
  id += buf_low; 
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Heltec.display->init();
  Heltec.display->flipScreenVertically();  
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->clear();  
  Heltec.display->drawString(0, 0, "Sender Active!");
  Heltec.display->drawString(0, 10, "Id: " + String(id));
  Heltec.display->display();
  LoRa.setSpreadingFactor(7);
  LoRa.setSignalBandwidth(BW);
  Serial.begin(115200);
  wifi_sniffer_init();
  pinMode(LED_GPIO_PIN, OUTPUT);
  Serial.println(id);
}

void loop() {
  vTaskDelay(WIFI_CHANNEL_SWITCH_INTERVAL / portTICK_PERIOD_MS);
  wifi_sniffer_set_channel(channel);
  channel = (channel % WIFI_CHANNEL_MAX) + 1;
}
