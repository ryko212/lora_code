# LoRa_Code

Repository for code files for the LoRa radios.

Be sure to create "WiFiConfig.h" in the Sniffer_Receiver folder with the following format:

```
const char *ServerName = "add server post url here"
const char *SSID = "SSID Name";
const char *WiFiPassword = "SSID Password";
```
